## Parliament Import
This is a simple utility for importing datasets into the [Parliament](http://parliament.semwebcentral.org/) triple store.

### How to build
To build a self-contained JAR file use the following Maven command:

```bash
mvn install:install-file compile assembly:single
```

The resulting JAR (let's call it `parliament-import.jar`) will be generated in the `target` subdirectory.

### Assumptions

The utility assumes that there is a working instance of `Parliament` available on [http://localhost:8089/parliament/](http://localhost:8089/parliament/).

### Invoking the utility:

```bash
java -jar parliament-import.jar --graph=<graph-name> (--dir|--file)=<path>
```

where `<path>` is an absolute path (i.e., not containing `.` and `..`) to either a single RDF file or a directory (with RDF files to import).

#### Example:

```bash
java -jar parliament-import.jar --graph=http://mydataset.org --dir=dataset
```

#### Remarks

  - At the moment the utility, upon importing a dataset, **removes** any provious content of the target graph.
