package eu.interiot.utils;

import com.bbn.parliament.jena.joseki.client.RemoteModel;
import com.hp.hpl.jena.rdf.model.Model;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import org.apache.jena.riot.RDFDataMgr;

import java.io.File;
import java.io.IOException;

public class ParliamentImport {

    public static void main(String[] args) {
        OptionParser parser = new OptionParser();
        parser.accepts( "graph" ).withRequiredArg();
        parser.accepts( "file" ).withRequiredArg();
        parser.accepts( "dir" ).withOptionalArg();
        OptionSet options = parser.parse(args);
        if (options.has("graph")) {
            String graph = options.valueOf("graph").toString();
            String sparqlEndpoint = "http://localhost:8089/parliament/sparql";
            String bulkEndpoint = "http://localhost:8089/parliament/bulk";
            RemoteModel rmParliament = new RemoteModel(sparqlEndpoint, bulkEndpoint);
            try {
                rmParliament.dropNamedGraph(graph);
                rmParliament.createNamedGraph(graph, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (options.has("dir")) {
                String dir = options.valueOf("dir").toString();
                File folder = new File(dir);
                File[] listOfFiles = folder.listFiles();
                if (listOfFiles != null) {
                    Model m;
                    System.out.print("\nImporting...");
                    for (File file : listOfFiles) {
                        if (file.isFile()) {
                            m = RDFDataMgr.loadModel(file.getAbsolutePath());
                            try {
                                rmParliament.insertStatements(m, graph);
                            } catch (IOException e) {
                                System.out.println(file.getAbsolutePath());
                                e.printStackTrace();
                            }
                        }
                    }
                    System.out.println("\nDone.\n");
                }
            } else if (options.has("file")) {
                String filename = options.valueOf("file").toString();
                File file=new File(filename);
                Model m;
                if (file.isFile()) {
                    m = RDFDataMgr.loadModel(file.getAbsolutePath());
                    System.out.print("\nImporting...");
                    try {
                        rmParliament.insertStatements(m, graph);
                    } catch (IOException e) {
                        System.out.println(file.getAbsolutePath());
                        e.printStackTrace();
                    }
                    System.out.println("\nDone.\n");
                } else {
                    System.out.println("\nPath \"" + filename + "\" does not specify a file!\n");
                    System.exit(1);
                }
            } else {
                System.out.println("\nYou need to specify an abolute path to either a directory or a file\n");
                System.out.println("    --dir=/full/path/to/dir");
                System.out.println("    --file=/full/path/to/file\n");
                System.exit(1);
            }
        } else {
            System.out.println("\nUsage – required options:\n");
            System.out.println("    --graph=graph-name\n");
            System.out.println("together with one of:\n");
            System.out.println("    --dir=/path/to/directory");
            System.out.println("    --file=/path/to/file\n");
            System.exit(1);
        }
    }
}
